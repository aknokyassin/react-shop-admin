import React from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

export default function Categorieupdate() {
  return (
    <div>
    <h1> Modifier Catégorie</h1>
    <div className="formnewcategorie">
      <form >
      <div className="inputnewcate">
      <TextField fullWidth  id="standard-basic" label="Standard" variant="standard" />
      </div>
      <div className="inputcolor">
      <input type="color" />
      </div>
        <div className="btnnewcategorieform">
      <Button variant="outlined">Outlined</Button>

        </div>
      </form>
    </div>
  </div>
  )
}
