import React ,{ useState } from 'react'
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import useFetch from "../../hooks/useFetch";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';


export default function Newcategorie() {


  return (
    <div>
      <h1> Nouvelle Catégorie</h1>
      <div className="formnewcategorie">
        <form >
        <div className="inputnewcate">
        <TextField fullWidth  id="standard-basic" label="Standard" variant="standard" />
        </div>
        <div className="inputcolor">
        <input type="color" />
        </div>
          <div className="btnnewcategorieform">
        <Button variant="outlined">Outlined</Button>

          </div>
        </form>
      </div>
    </div>
  )
}


