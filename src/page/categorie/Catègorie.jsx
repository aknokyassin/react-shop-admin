import React,{useState} from "react";
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import useFetch from '../../hooks/useFetch'
function Catègorie(){
    const {list} = useFetch();
    const [categorie , setCategorie]=useState([
        {id:1,title:"Ordinateur" , couleur :"red" },
        {id:2,title:"Phone" , couleur :"green" },
      ])
    
    //   useEffect(() => {
    //     list('categories').then((data) =>{
    //         setCategorie(data)
    //     })

    //   }, []);
    return( 
    <div className="categorie">
        <div className="headcategorie">
        <h1>categorie des Produits </h1>
        <Link to='/categorie/newcategorie' className="btnnewcategorie">new categorie</Link> 
        </div>
            
            <div className="categorielist"> 
                <body className="" >
                <div className='table'>
                <TableContainer >
                <Table aria-label="simple table">
                    <TableHead>
                    <TableRow className='headtable'>
                        <TableCell>Nom </TableCell>
                        <TableCell >Couleur</TableCell>
                        <TableCell ></TableCell>
                        <TableCell ></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {categorie ? categorie.map((cate)=>(
                        <TableRow key={cate.id} className="rowtable">
                        <TableCell  scope="row">
                        <Link className='nameprod' to={`/categorie/categoriedetails${cate.id}`}>{cate.title}</Link>
                        </TableCell>
                        <TableCell><div className='color' style={{ backgroundColor: `${cate.couleur}` }}></div></TableCell>
                        <TableCell align="right"><Link className='btnupdate' to={`/categorie/updatecategorie${cate.id}`}>Update</Link></TableCell>
                        <TableCell align="right">
                        <Button variant="contained" color="error" >Delete</Button></TableCell> 
                    </TableRow>
                    )):"error"}
                    </TableBody>
                </Table>
                </TableContainer>
                </div>
                </body>
            </div>
    </div>

    );
}

export default Catègorie;