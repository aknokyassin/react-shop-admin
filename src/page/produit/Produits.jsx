import React,{useState} from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

export default function Produits() {

  return (
    <div>
      <h1> list des produits</h1>
      <div className='btnnewcategorieform'>
      {/* <Button variant="contained" color="error" >noveau produit</Button> */}
      <Button  >
         <Link to='/produit/nouvelleproduit' className="btnnewproduit">noveau produit</Link>
         </Button>
      {/* <Link to='/produit/produits' */}
      </div>
      <div>
<TableContainer >
                <Table aria-label="simple table">
                    <TableHead>
                    <TableRow className='headtable'>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell>Titre</TableCell>
                        <TableCell ></TableCell>
                        <TableCell >prix</TableCell>
                        <TableCell >stock</TableCell>
                        <TableCell ></TableCell>
                        <TableCell ></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow  className="rowtable">
                        <TableCell >123</TableCell>
                        <TableCell><div className='color' >img</div></TableCell>
                        <Link to='/produit/produitdetail'> <TableCell >PC ASUS 1</TableCell></Link>
                        <TableCell ></TableCell>
                        <TableCell >4000 mad</TableCell>
                        <TableCell >120</TableCell>
                        <Link to='/produit/Produitupdate'><TableCell ><Button variant="contained" className='btn btn-primary' >modifier</Button></TableCell> </Link>
                        <TableCell ><Button variant="contained" color="error" >Delete</Button></TableCell> 
                    </TableRow>
                    <TableRow  className="rowtable">
                        <TableCell >124</TableCell>
                        <TableCell><div className='color' >img</div></TableCell>
                        <Link to='/produit/Produitdetail'><TableCell >mac pro</TableCell></Link>
                        <TableCell ></TableCell>
                        <TableCell >12000 mad</TableCell>
                        <TableCell >10</TableCell>
                        <Link  to='/produit/Produitupdate'><TableCell ><Button className='btn btn-pr imary'variant="contained" >modifier</Button></TableCell> </Link>
                        <TableCell ><Button variant="contained" color="error" >Delete</Button></TableCell> 
                    </TableRow>
                    </TableBody>
                </Table>
                </TableContainer>
      </div>
    </div>
  )
}
