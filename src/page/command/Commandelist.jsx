import React from "react";
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
function Commandelist(){
    return( 
        <div>
        <h1> List des Commande</h1>
        <div className="btnnewcategorieform">
        <Link to='/command/Newcommande' className="btnnewcategorie">novvellecommande</Link>          
          </div>
          <div>
          <TableContainer >
                <Table aria-label="simple table">
                    <TableHead>
                    <TableRow className='headtable'>
                        <TableCell>numero</TableCell>
                        <TableCell>nombre produit</TableCell>
                        <TableCell>prix total</TableCell>
                        <TableCell >statut</TableCell>
                        <TableCell ></TableCell>
                        
                        <TableCell ></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow  className="rowtable">
                        <TableCell >123</TableCell>
                        <TableCell>2produit</TableCell>
                        <TableCell >3000mad</TableCell>
                        <TableCell >transfairer</TableCell>
                        <TableCell ><Button variant="contained" className='btn btn-primary' >modifier</Button></TableCell> 
                        <TableCell ><Button variant="contained" color="error" >Delete</Button></TableCell> 
                    </TableRow>
                    <TableRow  className="rowtable">
                    <TableCell >123</TableCell>
                        <TableCell>2produit</TableCell>
                        <TableCell >3000mad</TableCell>
                        <TableCell > en attente</TableCell>
                        <TableCell ><Button variant="contained" className='btn btn-primary' >modifier</Button></TableCell> 
                        <TableCell ><Button variant="contained" color="error" >Delete</Button></TableCell> 
                    </TableRow>
                    
                    <TableRow  className="rowtable">
                    <TableCell >123</TableCell>
                        <TableCell>2produit</TableCell>
                        <TableCell >3000mad</TableCell>
                        <TableCell > Annuler</TableCell>
                        <TableCell ><Button variant="contained" className='btn btn-primary' >modifier</Button></TableCell> 
                        <TableCell ><Button variant="contained" color="error" >Delete</Button></TableCell> 
                    </TableRow>
                    </TableBody>
                </Table>
                </TableContainer>
          </div>
        </div>
    );
}

export default Commandelist;