import React from "react";
import Catègorie from "./page/categorie/Catègorie";
import Commande from "./page/command/Commandelist";
import Produits from "./page/produit/Produits";
import Utilisateur from "./page/utilisteur/Utilisateur";
import './App.css';
import { BrowserRouter, Link, NavLink, Route, Routes } from 'react-router-dom';
import Newcategorie from "./page/categorie/Newcategorie";
import Categoriedetails from "./page/categorie/Categoriedetails";
import Categorieupdate from "./page/categorie/Categorieupdate";
import Produitdetail from "./page/produit/produitdetail";
import Produitupdate from "./page/produit/produitupdate";
import Noveauproduit from "./page/produit/noveauproduit";
import Commandelist from "./page/command/Commandelist";
import Newcommande from "./page/command/Newcommande";

function App(){
    return(
      <BrowserRouter>
    <div className='cont'>
    <div className='navbar'>
        <img src="" alt="" className='img' />
            <ul className='pages'>
            <li>
            <NavLink to={'/categorie'} className="li" >Catégorie</NavLink >
            </li>
            <li>
            <NavLink to={'/Produits'} className="li" >Produits</NavLink >
            </li>
            <li>
            <NavLink to={'/Commandes'} className="li" >Commandes</NavLink >
            </li>
            <li>
            <NavLink to={'/Utilisateurs'} className="li" >Utilisateurs</NavLink >
            </li>
            </ul>
      </div>
      <div className='body'>
        <div className='content'>
          <Routes>
            <Route path='/Produits' element={<Produits/>}/>
            <Route path='/Commandes' element={<Commande/>}/>
            <Route path='/Utilisateurs' element={<Utilisateur/>}/>

            <Route path='/categorie' element={<Catègorie/>}/>
            <Route path="/categorie/new" element={<Newcategorie/>}/>
            <Route path="/categorie/:id/details" element={<Categoriedetails/>}/>
            <Route path="/categorie/:id/update" element={<Categorieupdate/>}/>
            
            <Route path="/produit/Produitdetail"element={<Produitdetail/>}/>
            <Route path="/produit/Produitupdate"element={<Produitupdate/>}/>
            <Route path="/produit/nouvelleproduit"element={<Noveauproduit/>}/>
            <Route path="/command/Newcommande"element={<Newcommande/>}/>
            
            <Route path="/commande/Commandelist"element={<Commandelist/>}/>
            
            
          </Routes>
          
        </div>
      </div>
        
    </div>
    </BrowserRouter>
    );
}

export default App